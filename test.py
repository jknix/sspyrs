
def reporttest_noparam(links, uname, pwd):
    import sspyrs

    # Create report objects
    reportobjs = [sspyrs.report(x, uname, pwd) for x in links]

    # Get links
    genlinks = [x.exportlink for x in reportobjs]

    # If don't get 4 links, got an issue
    if len(genlinks) != len(links):
        return 'Error in link information'

    # If raw data does not return dictionary, gotsa issue
    reportrawdata = [x.rawdata() for x in reportobjs]
    rawdatacheck = all([isinstance(x, dict) for x in reportrawdata])

    if not rawdatacheck:
        return 'Error in raw data dictionary'

    # If table data aint no dict package no go
    tabledata = [x.tabledata() for x in reportobjs]
    tabledatacheck = all([isinstance(x, dict) for x in tabledata])

    if not tabledatacheck:
        return 'Error in table data return'

    # Check a table in each return
    firsttables = [list(x.keys())[0] for x in tabledata]

    # Check indexes
    indexchecks = all([isinstance(x[y], object) for x, y in zip(tabledata, firsttables)])
    if not indexchecks:
        return 'Got an issue with indexes of dataframes'

    return 0


def reporttest_directdown(link, uname, pwd):
    import sspyrs
    from os import path, listdir, remove
    rpt = sspyrs.report(link, uname, pwd)

    oldfiles = listdir('tmpout')
    [remove('tmpout/' + x) for x in oldfiles]

    formats = ['PDF',
               'Excel',
               'Word',
               'XML',
               'IMAGE',
               'PPTX',
               'CSV',
               'ATOM']

    [rpt.directdown('tmpout/myfile', x) for x in formats]

    newfiles = listdir('tmpout')
    targetfiles = ['myfile.atomsvc',
                   'myfile.csv',
                   'myfile.doc',
                   'myfile.pdf',
                   'myfile.pptx',
                   'myfile.tiff',
                   'myfile.xls',
                   'myfile.xml']

    test = newfiles == targetfiles
    if test:
        return 0
    else:
        return 'Summin screwed up'



def reporttest_param(link, uname, pwd, params):
    import sspyrs

    # Create report objects
    reportobj = sspyrs.report(link, uname, pwd, parameters=params)

    td = reportobj.tabledata()

    if type(td) == dict:
        return [0,td]
    else:
        return 'Issue on params'






import test_config

assert reporttest_noparam(test_config.tablelinks, test_config.uname, test_config.pw) == 0
assert reporttest_directdown(test_config.tablelinks[0], test_config.uname, test_config.pw) == 0
assert reporttest_param(test_config.link_forparams,
                        test_config.uname,
                        test_config.pw,
                        test_config.params) == 0

assert reporttest_param(test_config.link_forparams,
                        test_config.uname,
                        test_config.pw,
                        test_config.params_multi) == 0
